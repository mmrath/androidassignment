package com.mmrath.android.assignment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Activity which displays a login screen to the user, offering registration as
 * well.
 */
public class LoginActivity extends Activity {


    // Values for email and password at the time of the login attempt.
    private String mEmail;
    private String mPassword;
    private boolean mRememberMe;

    // UI references.
    private EditText mEmailView;
    private EditText mPasswordView;
    private CheckBox mRememberMeView;

    //To show error message
    private TextView mLoginStatusView;
    private View mFocusView;
    private AuthenticationService authenticationService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        authenticationService = new AuthenticationService(this);

        //If user is already authenticated then directly go to main screen
        if(authenticationService.isAuthenticated()){
            startMainActivity(Global.APP_OPEN_MODE_RETURN);
            return;
        }
        setContentView(R.layout.activity_login);

        // Set up the login form.
        mEmail = getIntent().getStringExtra(Global.LAST_SIGNED_IN_USER);
        mEmailView = (EditText) findViewById(R.id.etEmail);
        mEmailView.setText(mEmail);

        mPasswordView = (EditText) findViewById(R.id.etPassword);
        mRememberMeView = (CheckBox) findViewById(R.id.cbRememberMe);
        mLoginStatusView = (TextView) findViewById(R.id.login_status_message);

        findViewById(R.id.btnSignIn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        findViewById(R.id.btnRegister).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptRegister();
            }
        });
    }

    /**
     * This method validates the form and then register the user
     */
    private void attemptRegister() {

        boolean isFormValid = validateForm(false);
        if (!isFormValid) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            if (mFocusView != null) mFocusView.requestFocus();
        } else {

            Log.i(Global.TAG, "Registering user.");
            //Store the logged in user
            authenticationService.register(mEmail, mPassword);
            startMainActivity(Global.APP_OPEN_MODE_REGISTER);
        }

    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    public void attemptLogin() {

        boolean isFormValid = validateForm(false);
        if (!isFormValid) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            mFocusView.requestFocus();
        } else {

            String password = authenticationService.getPassword(mEmail);
            if (!TextUtils.isEmpty(password) && password.equals(mPassword)) {
                Log.i(Global.TAG, "User credentials valid.");
                //Store the logged in user
                authenticationService.setAuthenticated(mEmail, mRememberMe);
                //Start main screen
                startMainActivity(Global.APP_OPEN_MODE_SIGN_IN);
            } else {
                Log.i(Global.TAG, "User credentials invalid.");
                //Show error
                mLoginStatusView.setText(R.string.error_incorrect_credentials);
                mLoginStatusView.setVisibility(View.VISIBLE);
            }
        }
    }

    /**
     * Validates the sign in/register form
     *
     * @param register whether the form is being submitted for registration
     * @return true if form is valid
     */
    private boolean validateForm(boolean register) {

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        mEmail = mEmailView.getText().toString();
        mPassword = mPasswordView.getText().toString();
        mRememberMe = mRememberMeView.isChecked();
        mLoginStatusView.setVisibility(View.GONE);

        boolean isValidForm = true;

        // Check for a valid password.
        if (TextUtils.isEmpty(mPassword)) {
            mPasswordView.setError(getString(R.string.error_field_required));
            mFocusView = mPasswordView;
            isValidForm = false;
        } else if (mPassword.length() < 4) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            mFocusView = mPasswordView;
            isValidForm = false;
        }

        AuthenticationService authenticationService = new AuthenticationService(this);
        // Check for a valid email address.
        if (TextUtils.isEmpty(mEmail)) {
            mEmailView.setError(getString(R.string.error_field_required));
            mFocusView = mEmailView;
            isValidForm = false;
        } else if (!mEmail.contains("@")) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            mFocusView = mEmailView;
            isValidForm = false;
        } else if (register && authenticationService.isUserExists(mEmail)) {
            mEmailView.setError(getString(R.string.error_register_user_exists));
            mFocusView = mEmailView;
            isValidForm = false;
        }
        return isValidForm;
    }

    private void startMainActivity(int openMode){
        //Start main screen
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        i.putExtra(Global.APP_OPEN_MODE_KEY, openMode);
        startActivity(i);
        //Finish current activity so that user will not come back to sign in screen
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.login, menu);
        return true;
    }

}
