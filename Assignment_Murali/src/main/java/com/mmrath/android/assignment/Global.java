package com.mmrath.android.assignment;

/**
 * Created by Murali on 7/7/13.
 * Class to hold global constants
 */
public class Global {
    //Preferences for application
    public static final String SECURITY_PREFERENCES_NAME = "MuraliAssignmentPreferences";

    //Tag for logging
    public static final String TAG = "MURALI_ASSIGNMENT";

    //postfix to password key
    public static final String PASSWORD_KEY_POSTFIX = "$PASS$";

    //Key to hold last signed in user
    public static final String LAST_SIGNED_IN_USER = "$loggedInUser$";

    //Preferences key to say if the user wanted to remain login
    public static final String SIGNED_IN = "$loggedIn$";

    //Key to say if user choose to signed in
    public static final String REMEMBER_ME = "$rememberMe$" ;


    //Key to send value to main activity indicating mode of opening main activity
    public static final String APP_OPEN_MODE_KEY = "$AppOpenMode$";
    public static final int APP_OPEN_MODE_SIGN_IN = 1; //User signed in
    public static final int APP_OPEN_MODE_RETURN = 2; //User had earlier select to remain signed in
    public static final int APP_OPEN_MODE_REGISTER = 3; //User just registered



}
