package com.mmrath.android.assignment;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends Activity {

    private TextView mWelcomeMessageView;
    private AuthenticationService authenticationService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mWelcomeMessageView = (TextView)findViewById(R.id.tvWelcomeMessage);
        authenticationService = new AuthenticationService(this);
        int openMode = getIntent().getIntExtra(Global.APP_OPEN_MODE_KEY,0);
        setWelcomeText(openMode);
        findViewById(R.id.btnSignOut).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Sign out
                authenticationService.signOut();

                //Start login activity
                Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(i);
                //Finish current activity so that user will not come back to sign in screen
                finish();
            }
        });
    }


    private void setWelcomeText(int openMode) {
        String message;
        switch (openMode){
            case Global.APP_OPEN_MODE_SIGN_IN:
                message = getResources().getString(R.string.welcome_sign_in);
                break;
            case Global.APP_OPEN_MODE_REGISTER:
                message = getResources().getString(R.string.welcome_register);
                break;
            case Global.APP_OPEN_MODE_RETURN:
                message = getResources().getString(R.string.welcome_return);
                break;
            default:
                message = "Sorry %s! I don't know how you landed here";
                break;
        }
        mWelcomeMessageView.setText(String.format(message, authenticationService.getSignedInUser()));
        mWelcomeMessageView.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //If remember me option is not checked then sign out when app closes
        if( !authenticationService.isRememberMe())
        {
            authenticationService.signOut();
        }
    }
}
