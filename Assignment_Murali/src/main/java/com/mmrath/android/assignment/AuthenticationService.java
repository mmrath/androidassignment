package com.mmrath.android.assignment;


import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

public class AuthenticationService {
    private Context context;

    public AuthenticationService(Context context){
        this.context = context;
    }

    public String getPassword(String username){
        //Get password from preferences
        SharedPreferences settings = context.getSharedPreferences(Global.SECURITY_PREFERENCES_NAME, 0);
        return settings.getString(username.toLowerCase()+Global.PASSWORD_KEY_POSTFIX, "");
    }

    public boolean isUserExists(String username){
        SharedPreferences settings = context.getSharedPreferences(Global.SECURITY_PREFERENCES_NAME, 0);
        return !TextUtils.isEmpty(settings.getString(username.toLowerCase(), ""));
    }

    public void register(String username, String password){
        SharedPreferences settings = context.getSharedPreferences(Global.SECURITY_PREFERENCES_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();

        //Write username and password
        editor.putString(username.toLowerCase(), username.toLowerCase());
        editor.putString(username.toLowerCase() + Global.PASSWORD_KEY_POSTFIX, password);
        //Commit the edits!
        editor.commit();
    }

    public void setAuthenticated(String username, boolean rememberMe){
        //User has successfully logged in, save this information
        //We need an Editor object to make preference changes.
        SharedPreferences settings = context.getSharedPreferences(Global.SECURITY_PREFERENCES_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();

        editor.putString(Global.LAST_SIGNED_IN_USER, username.toLowerCase());
        editor.putBoolean(Global.SIGNED_IN, true);
        editor.putBoolean(Global.REMEMBER_ME, rememberMe);
        //Commit the edits!
        editor.commit();
    }

    public boolean isAuthenticated(){
        SharedPreferences settings = context.getSharedPreferences(Global.SECURITY_PREFERENCES_NAME, 0);
        //Get "hasLoggedIn" value. If the value doesn't exist yet false is returned
        boolean hasLoggedIn = settings.getBoolean(Global.SIGNED_IN, false);
        return hasLoggedIn;
    }

    /**
     * returns the signed in user
     */
    public String getSignedInUser() {
        SharedPreferences settings = context.getSharedPreferences(Global.SECURITY_PREFERENCES_NAME, 0);
        return settings.getString(Global.LAST_SIGNED_IN_USER, "");
    }

    public void signOut() {
        //Reset sign in status
        SharedPreferences settings = context.getSharedPreferences(Global.SECURITY_PREFERENCES_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();

        editor.remove(Global.LAST_SIGNED_IN_USER);
        editor.remove(Global.SIGNED_IN);
        editor.remove(Global.REMEMBER_ME);
        //Commit the edits!
        editor.commit();
    }

    public boolean isRememberMe(){
        SharedPreferences settings = context.getSharedPreferences(Global.SECURITY_PREFERENCES_NAME, 0);
        return settings.getBoolean(Global.REMEMBER_ME, false);
    }
}
